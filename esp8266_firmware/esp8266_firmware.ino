
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <OneWire.h>
#include <DS18B20.h>

ESP8266WiFiMulti WiFiMulti;

const char* ssid     = "TP-Link_856C";
const char* password = "1122334455";

#define ONE_WIRE_BUS D5

OneWire oneWire(ONE_WIRE_BUS);
DS18B20 sensor(&oneWire);

void setup(void)
{
  Serial.begin(115200);

  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);
  
  Serial.println(__FILE__);
  Serial.print("DS18B20 Library version: ");
  Serial.println(DS18B20_LIB_VERSION);

  sensor.begin();
}

void loop(void)
{ 

  sensor.requestTemperatures();
  
  while (!sensor.isConversionComplete());  // wait until sensor is ready
  
  Serial.print("Temp: ");
  double temperature = sensor.getTempC();
  Serial.println(temperature);

  if ((WiFiMulti.run() == WL_CONNECTED)) {

    WiFiClient client;
    HTTPClient http;
    Serial.print("[HTTP] begin...\n");
    
    if (http.begin(client, "http://192.168.0.100/insert_temp.php?temperature=" + (String)temperature)) {  // HTTP

      //Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }
  }
  delay(3500);
  
}
