import telegram.ext
import mysql.connector
from mysql.connector import Error
import json
import csv

token = "5537963904:AAF9dDxcL9OlXlzNXCHACMUYkD5Q_SC6rtM"

welcomeMessage="Welcome to the bot. Let's get started"
helpMessage="/last - get last temperature\n/min - get min temperature\n/max - get max temperature\n/export - export to .csv file"
databaseMessages={
        'DatabaseConnection':"Can't connect to database",
        'ConnectionClosed':"Connection is closed",
        'Connected':"Connection is established"
}

fetchLastTemperature = "SELECT temp_value, reading_time FROM tbl_temp order BY temp_id DESC limit 1;"
fetchMaxTemperature = "SELECT temp_value, reading_time FROM tbl_temp WHERE temp_value = (SELECT MAX(temp_value) FROM tbl_temp) order BY reading_time DESC limit 1;"
fetchMinTemperature = "SELECT temp_value, reading_time FROM tbl_temp WHERE temp_value = (SELECT MIN(temp_value) FROM tbl_temp) order BY reading_time DESC limit 1;"
fetchExportDB = "SELECT * FROM tbl_temp"

connection = {}

try:
    connection = mysql.connector.connect(host='localhost',
                                         database='db_esp32',
                                         user='esp',
                                         password='esp12345')
    if connection.is_connected():
            db_Info = connection.get_server_info()
            print(databaseMessages['Connected'], db_Info)
            cursor = connection.cursor()
except Error as e:
    print(databaseMessages['DatabaseConnection'], r)

def start(update, context):
    update.message.reply_text(welcomeMessage)

def help(update,context):
    update.message.reply_text(helpMessage)

def getLastTemperature(update, context):
        cursor.execute(fetchLastTemperature)
        record = cursor.fetchone()
        connection.commit()
        update.message.reply_text("Temperature: " + str(record[0]) + " °C\nTime: " + str(record[1]))
                
def getMaxTemperature(update, context):
        cursor.execute(fetchMaxTemperature)
        record = cursor.fetchone()
        connection.commit()
        update.message.reply_text("Temperature: " + str(record[0]) + " °C\nTime: " + str(record[1]))

def getMinTemperature(update, context):
        cursor.execute(fetchMinTemperature)
        record = cursor.fetchone()
        connection.commit()
        update.message.reply_text("Temperature: " + str(record[0]) + " °C\nTime: " + str(record[1]))

def exportToCSV(update, context):
        cursor.execute(fetchExportDB)
        res = cursor.fetchall();
        connection.commit()
        with open ("export.csv", "w", newline='') as file:
            for row in res:
                csv.writer(file).writerow(row)
        chat_id = update.message.chat_id
        document = open("export.csv", "rb")
        context.bot.send_document(chat_id, document)
        

updater = telegram.ext.Updater(token, use_context=True)
disp = updater.dispatcher

disp.add_handler(telegram.ext.CommandHandler("start",start))
disp.add_handler(telegram.ext.CommandHandler("help",help))
disp.add_handler(telegram.ext.CommandHandler("last",getLastTemperature))
disp.add_handler(telegram.ext.CommandHandler("max",getMaxTemperature))
disp.add_handler(telegram.ext.CommandHandler("min",getMinTemperature))
disp.add_handler(telegram.ext.CommandHandler("export",exportToCSV))

updater.start_polling()
updater.idle()