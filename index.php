<!DOCTYPE html>
<html><body>
<?php

   $servername = "localhost";
   $username = "esp";
   $password = "esp12345";
   $database_name = "db_esp32";

$connection = new mysqli($servername, $username, $password, $database_name);
if ($connection->connect_error) {
    die("Connection failed: " .$connection->connect_error);
} 

$sql = "SELECT temp_id, temp_value, reading_time FROM tbl_temp order BY temp_id DESC";

echo '<table cellspacing="3" cellpadding="3">
      <tr> 
        <td>ID</td> 
        <td>Value</td> 
        <td>Timestamp</td> 
      </tr>';
 
if ($result = $connection->query($sql)) {
    while ($row = $result->fetch_assoc()) {
        $row_id = $row["temp_id"];
        $row_value1 = $row["temp_value"];
        $row_reading_time = $row["reading_time"];

      
        echo '<tr> 
                <td>' . $row_id . '</td> 
                <td>' . $row_value1 . '</td> 
                <td>' . $row_reading_time . '</td> 
              </tr>';
    }
    $result->free();
}

$connection->close();
?> 
</table>
</body>
</html>
