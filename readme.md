## Map
![map](https://gitlab.com/maxim.kc/mkrdkr/-/raw/main/map.png)

## Installation

[MySQL Server](https://www.mysql.com/)

[Apache Lounge](https://www.apachelounge.com/download/)

[PHP](https://www.php.net/)

Для створення боту вирористовувалась бібліотека [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot).

```bash
python pip install python-telegram-bot
```
Для підключення до БД використовувалась бібліотека mysql-connector-python
```bash
python pip install mysql-connector-python
```

## Usage

Telegram bot - [@esp8266temperature_bot](https://t.me/esp8266temperature_bot)

Web-server - [178.95.177.213](http://178.95.177.213/index.php)

DataBase - 178.95.177.213:3030